package br.com.lead.collector.repositories;

import br.com.lead.collector.models.Produto;
import org.springframework.data.repository.CrudRepository;

//nesse caso o Integer é o tipo da chave primaria id
public interface ProdutoRepository extends CrudRepository<Produto, Integer> {


}
