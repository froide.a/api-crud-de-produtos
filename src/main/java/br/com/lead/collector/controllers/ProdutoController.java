package br.com.lead.collector.controllers;


import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;


@RestController
//controle de entrada e saida dos dados
@RequestMapping("/produtos")
//nome da url amigavel
public class ProdutoController  {

    @Autowired
    private ProdutoService produtoService;

    @PostMapping
    // utilizado para insert no bando de dados
    @ResponseStatus(HttpStatus.CREATED)
    public Produto cadastrarProduto(@RequestBody @Valid Produto produto) {
        //@requestbody é para receber os dados do json
        //@valid efetua as validacoes da classe produto
        Produto objetoProduto = produtoService.salvarProduto(produto);
        return objetoProduto;
    }


    @GetMapping
    public Iterable<Produto> lerTodosOsProdutos() {
        return produtoService.lerTodosOsProdutos();
    }

    //recebe na url o id - usa-se { }
    @GetMapping("/{id}")
    public Produto pesquisarPorId(@PathVariable(name = "id") int id) {
        try {
            Produto produto = produtoService.buscarProdutoPeloId(id);
            return produto;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Produto atualizarProduto(@RequestBody @Valid Produto produto, @PathVariable(name = "id")int id) {
        try {
            Produto produtoDB = produtoService.atualizarProduto(id, produto);
            return produtoDB;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarProduto(@PathVariable(name = "id") int id){
        try{
            produtoService.deletarProduto(id);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}