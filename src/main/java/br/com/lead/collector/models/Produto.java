package br.com.lead.collector.models;

import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.*;

//mapeia para a tabela
@Table(name= "produtos")
@Entity
public class Produto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Nome do Produto é Obrigatório")
    @NotBlank(message = "Prencha o Nome do Produto")
    @Size(min = 3, message = "Nome no minimo com 3 caracteres")
    private String nome;

    @NotNull(message = "Descrição do produto é Obrigatória")
    @NotBlank(message = "Preencha a descrição do produto")
    @Size(min = 3, message = "Descrição no minimo com 5 caracteres")
    private String descricao;

    @NotNull(message = "Preço Obrigatório")
    @Digits(integer = 6, fraction = 2 )
    @DecimalMin(value = "1.00", message = "valor minimo é um 1.00")
    private double preco;

    // contrutor vazio para receber os dados do json
    public Produto() {
    }

    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public double getPreco() {
        return preco;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }


}